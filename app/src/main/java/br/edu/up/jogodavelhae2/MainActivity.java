package br.edu.up.jogodavelhae2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    ArrayList<View> botoes;
    ImageView imgLinha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgLinha = (ImageView) findViewById(R.id.imgLinha);
        LinearLayout linha1 = (LinearLayout) findViewById(R.id.linha1);
        LinearLayout linha2 = (LinearLayout) findViewById(R.id.linha2);
        LinearLayout linha3 = (LinearLayout) findViewById(R.id.linha3);

        botoes = new ArrayList<>();
        botoes.addAll(linha1.getTouchables());
        botoes.addAll(linha2.getTouchables());
        botoes.addAll(linha3.getTouchables());
    }

    public void onClickReiniciar(View v){
        imgLinha.setImageResource(R.drawable.fundo_transparente);
        for (View view : botoes) {
            ImageView imageView = (ImageView) view;
            imageView.setImageResource(R.drawable.btn_branco);
            imageView.setEnabled(true);
        }

        jogadas = new char[9];
    }

    char jogadorDaVez = 'o';
    char[] jogadas = new char[9];
    int[][] sequencias = {
            {1, 2, 3}, {4, 5, 6}, {7, 8, 9},
            {1, 4, 7}, {2, 5, 8}, {3, 6, 9},
            {1, 5, 9}, {3, 5, 7}
    };

    Map<Integer, Integer> linhas = new HashMap<>();
    {
        linhas.put(0, R.drawable.linha_horizontal_1);
        linhas.put(1, R.drawable.linha_horizontal_2);
        linhas.put(2, R.drawable.linha_horizontal_3);
        linhas.put(3, R.drawable.linha_vertical_1);
        linhas.put(4, R.drawable.linha_vertical_2);
        linhas.put(5, R.drawable.linha_vertical_3);
        linhas.put(6, R.drawable.linha_decrescente);
        linhas.put(7, R.drawable.linha_crescente);
    }

    public void onClickJogar(View v){

        ImageView imageView = (ImageView) v;

        String tag = (String) imageView.getTag();
        int posicao = Integer.parseInt(tag);
        jogadas[posicao -1] = jogadorDaVez;

        for (int i = 0; i < sequencias.length; i++) {
            int[] sequencia = sequencias[i];
            if (jogadas[sequencia[0] -1] == jogadorDaVez &&
                    jogadas[sequencia[1] -1] == jogadorDaVez &&
                    jogadas[sequencia[2] -1] == jogadorDaVez){
                //acabou
                for (View view : botoes){
                    ImageView imgv = (ImageView) view;
                    imgv.setEnabled(false);
                }
                imgLinha.setImageResource(linhas.get(i));
            }
        }

        if (jogadorDaVez == 'o'){
            imageView.setImageResource(R.drawable.o_verde_hdpi);
            imageView.setEnabled(false);
            jogadorDaVez = 'x';
        } else {
            imageView.setImageResource(R.drawable.x_preto_hdpi);
            imageView.setEnabled(false);
            jogadorDaVez = 'o';
        }
    }
}